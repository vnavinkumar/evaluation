from django.db import models

class CsvUpload(models.Model):
    date_uploaded = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)

    def __unicode__(self):
        return u'%s - %s' % (self.title, self.date_uploaded)

# TODO - make internal name the primary key for quicker insertion?
class Customer(models.Model):
    internal_name = models.CharField(max_length=150, unique=True)
    name = models.CharField(max_length=100, null=True)
    primary_lip = models.ForeignKey('self',null=True)
    category = models.CharField(max_length=50,null=True)

    def __unicode__(self):
        return u'%s' % (self.internal_name)

class InvoiceItem(models.Model):
    invoice_number = models.CharField(max_length=50)
    invoice_date = models.DateField()
    transaction_type = models.CharField(max_length=50)
    product_type = models.CharField(max_length=100)
    quantity = models.DecimalField(null=True,max_digits=12, decimal_places=5)
    rate = models.DecimalField(null=True, max_digits=12, decimal_places=5)
    amount = models.DecimalField(null=True, max_digits=12, decimal_places=5)
    memo = models.TextField(null=True)
    customer = models.ForeignKey(Customer)
    orig_csv = models.ForeignKey(CsvUpload)
    is_processed = models.BooleanField(default=False)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    product_class = models.CharField(max_length=50)
    revenue_type = models.CharField(max_length=50)

    def __unicode__(self):
        return u'%s - %s %s' % (self.customer,self.revenue_type,self.amount)

class ClientMrr(models.Model):
    mrr = models.DecimalField(max_digits=12,decimal_places=5)
    customer = models.ForeignKey(Customer)
    month = models.DateField()
    classification = models.CharField(max_length=25)
    classification_value = models.DecimalField(null=True, max_digits=12, decimal_places=5)

    def __unicode__(self):
        return u'%s | %s | %s | %s' % (self.customer, self.month, self.mrr, self.classification)

from django import forms

# TODO - validate that this is the header of the file
# Date,Transaction Type,Num,Client,Product/Service,Memo/Description,Qty, Rate , Amount ,Type of Revenue,Type of Customer,Start Date,End Date,Product Class
class CsvUploadForm(forms.Form):
    title = forms.CharField(max_length=100)
    file = forms.FileField()

# Treatspace Developer Evaluation

This Django project calculates and displays monthly financial data.

---

### General Instructions: 
> The test is administered on Monday, 21st of August 2017 at 9:00 AM regional time.
> Please go through the instructions below for installation and the test questions.
> You have one day (24 hours) to complete the test.
> Once completed, please submit a pull request on or before 9:00 AM regional time of 22nd August 2017 (Tuesday).
> Our engineering team will evaluate your responses and get back to you shortly.

---

### Installation

1. Clone the repository onto your machine.  
2. Create a virtual environment.  
3. Install the requirements from requirements.txt.  
4. Migrate the database.  
5. Sample data can be found in test_data.sql, use this to fill the database with data.  
6. Run the project and log in with username: admin / password: test1234  


### Bugs/Error
There are a number of bugs in it:  

1. On the accounts tab, the customers are currently unordered. Order them alphabetically by name.
2. On the accounts tab, each customer's data is just the overall data. Filter each customer's data by their id.  
3. On the bar graphs tab, data is currently being displayed up to the current month. The data being displayed should end at the last full month.  
4. On the bar graphs tab, the second graph is not graphing the data. The data is being pulled from the database but not making it the graph. Find where the disconnect is so the graph displays.  
5. On the bar graphs tab, a data spreadsheet for each graph is not shown. Please write the function insertGrid function in Jquery.  
6. Clicking on Scatter Graph tab is calling a 404, it is disconnected and needs to be connected to ScatterView.  
7. Once Scatter Graph tab is connected, implement scatterGraph in jQuery to get a scatter chart to show.  

---
